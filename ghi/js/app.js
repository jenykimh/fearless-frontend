function createCard(title, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="col h-auto d-inline-block">
            <div class="card">
                <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${title}</h5>
                        <p class="card-text">${description}</p>
                        <p class="card-text"><small class="text-muted">${location}</small></p>
                        <div class="card-footer">
                            ${startDate} - ${endDate}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
  }
  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        alertWarning("failed to fetch conferences")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const startDateOne = new Date(details.conference.starts);
            const startDate = startDateOne.toLocaleDateString();
            const endDateOne = new Date(details.conference.ends);
            const endDate = endDateOne.toLocaleDateString();

            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, startDate, endDate, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
            }
          }
        }
    } catch (e) {
        alertWarning('failed to load data')
      // Figure out what to do if an error is raised
    }

  });

function alertWarning(message){
    const alertMessage = `
    <div class="alert alert-primary" role="alert">
        ${message}
    </div>
    `;
    document.body.innerHTML += alertMessage;
}
