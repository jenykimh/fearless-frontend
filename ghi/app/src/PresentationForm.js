import React, { useState, useEffect } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);

    const [formData, setFormData] = useState({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: ''
    });

    const fetchData = async () => {
      const response = await fetch('http://localhost:8000/api/conferences/');
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
      }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8000/api/conferences/${formData.conference}/presentations/`;
        delete formData.conference
        console.log(formData)
        const fetchConfig = {
            method: "post",
            //Because we are using one formData state object,
            //we can now pass it directly into our request!
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        presenter_name: '',
        presenter_email: '',
        company_name: '',
        title: '',
        synopsis: '',
        conference: ''
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
        ...formData,
        [inputName]: value
    });
  };

  return (
    <div className="container">
      <h1>New Presentation</h1>
      <form onSubmit={handleSubmit} id="create-presentation-form">


        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Presenter Name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
            <label htmlFor="name">Presenter Name</label>
        </div>

        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Presenter Email" required type="text" name="presenter_email" id="presenter_email" className="form-control" />
            <label htmlFor="name">Presenter Email</label>
        </div>

        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Company Name" required type="text" name="company_name" id="company_name" className="form-control" />
            <label htmlFor="name">Company Name</label>
        </div>

        <div className="form-floating mb-3">
            <input onChange={handleFormChange} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
            <label htmlFor="name">Title</label>
        </div>

        <div className="mb-3">
              <label htmlFor="synopsis">Synopsis</label>
              <textarea onChange={handleFormChange} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
        </div>

        <div className="mb-3">
              <select onChange={handleFormChange} required name="conference" id="conference" className="form-select">
                <option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                  )
                })}
              </select>
        </div>
            <button className="btn btn-primary">Create</button>
          </form>
    </div>
  );
}

export default PresentationForm;
